package com.bsuir.planetask.airports;

/**
 *
 * @author Polina
 *
 */
public class AirportResourses {
	/**
	 * id for airport.
	 */
	private int airportId;
	/**
	 * Semaphore for stop.
	 */
	private int semaphore;
	/**
	 * Name for stop.
	 */
	private String name;
	/**
	 * Number of buses which are on the airport on the current moment.
	 */
	private int currentPlaneCount;
	/**
	 * Number of people who are on the airport on the current moment.
	 */
	private int numberOfPeople;

	/**
	 * Constructor for new object.
	 *
	 * @param id1             for airport
	 * @param semaphore1      for airport
	 * @param name1           for airport
	 * @param curCountPlane   Number of planes which are on the airport on the
	 *                        current moment
	 * @param numberOfPeople1 Number of people who are on the airport on the current
	 *                        moment
	 */
	public AirportResourses(final int id1, final int semaphore1, final String name1, final int curCountPlane,
			final int numberOfPeople1) {
		this.airportId = id1;
		this.semaphore = semaphore1;
		this.name = name1;
		this.currentPlaneCount = curCountPlane;
		this.numberOfPeople = numberOfPeople1;
	}

	/**
	 * Function returns count of busses on the airport.
	 *
	 * @return currentBusCount number
	 */

	/**
	 * Function returns airport's id.
	 *
	 * @return stopId airport's id
	 */
	public int getAirportId() {
		return airportId;
	}

	/**
	 * Function sets airport's id.
	 *
	 * @param stopId1 new name
	 */
	public void setAirportId(final int stopId1) {
		this.airportId = stopId1;
	}

	/**
	 * Function sets CurrentCountOfBuses.
	 *
	 * @param currentPlaneCount1 new currentCountOfBuses
	 */
	public void setCurrentPlaneCount(final int currentPlaneCount1) {
		this.currentPlaneCount = currentPlaneCount1;
	}

	/**
	 * Function returns count of busses on the airport.
	 *
	 * @return currentBusCount number
	 */
	public int getCurrentPlaneCount() {
		return currentPlaneCount;
	}

	/**
	 * Function sets airport's name.
	 *
	 * @param name1 new name
	 */
	public void setName(final String name1) {
		this.name = name1;
	}

	/**
	 * Function sets NumberOfPeople.
	 *
	 * @param numberOfPeople1 new setNumberOfPeople
	 */
	public void setNumberOfPeople(final int numberOfPeople1) {
		this.numberOfPeople = numberOfPeople1;
	}

	/**
	 * Function sets airport's semaphore.
	 *
	 * @param semaphore1 new semaphore
	 */
	public void setSemaphore(final int semaphore1) {
		this.semaphore = semaphore1;
	}

	/**
	 * Function returns airport's CurrentCountOfPlanes.
	 *
	 * @return currentCountOfBuses on the stop
	 */
	public int getCurrentCountOfPlanes() {
		return currentPlaneCount;
	}

	/**
	 * Function returns airport's name.
	 *
	 * @return name name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Function returns numberOfPeople.
	 *
	 * @return numberOfPeople on the airport
	 */
	public int getNumberOfPeople() {
		return numberOfPeople;
	}

	/**
	 * Function returns airport's semaphore.
	 *
	 * @return semaphore number
	 */
	public int getSemaphore() {
		return semaphore;
	}

}
