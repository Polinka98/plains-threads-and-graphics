package com.bsuir.planetask.airports;

/**
 *
 * @author Polina
 *
 */
public class DataForAirport {

	/**
	 * Semaphore for airport.
	 */
	private int semaphore;
	/**
	 * Id for stop.
	 */
	private int airportId;
	/**
	 * Name for airport.
	 */
	private String name;

	/**
	 * Constructor for new object.
	 */
	public DataForAirport() {
	}

	/**
	 * Constructor for new object.
	 *
	 * @param id1        for airport
	 * @param name1      for airport
	 * @param semaphore1 for airport
	 */
	public DataForAirport(final int id1, final String name1, final int semaphore1) {
		super();
		this.airportId = id1;
		this.name = name1;
		this.semaphore = semaphore1;

	}

	/**
	 * Function returns airport's id.
	 *
	 * @return stopId id
	 */
	public int getId() {
		return airportId;
	}

	/**
	 * Function returns airport's semaphore.
	 *
	 * @return semaphore number
	 */
	public int getSemaphore() {
		return semaphore;
	}

	/**
	 * Function returns airport's name.
	 *
	 * @return name of airport
	 */
	public String getName() {
		return name;
	}

	/**
	 * Function sets airport's name.
	 *
	 * @param name1 new name
	 */
	public void setName(final String name1) {
		this.name = name1;
	}

	/**
	 * Function sets airport's semaphore.
	 *
	 * @param semaphore1 new semaphore
	 */
	public void setSemaphore(final int semaphore1) {
		this.semaphore = semaphore1;
	}

	/**
	 * Function sets airport's id.
	 *
	 * @param stopId1 new id
	 */
	public void setStopId(final int stopId1) {
		this.airportId = stopId1;
	}

}
