package com.bsuir.planetask.creator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bsuir.planetask.airports.AirportResourses;
import com.bsuir.planetask.airports.DataForAirport;
import com.bsuir.planetask.planethread.PlaneThread;
import com.bsuir.planetask.storage.AirportStorage;
import com.bsuir.planetask.storage.RouteStorage;
import com.bsuir.planetask.validation.IncorrectInputException;

/**
 *
 * @author Polina
 *
 */
public final class PlainCreator {
	/**
	 * Constructor for new object.
	 */
	private PlainCreator() {
	}

	/**
	 *
	 * @param bigArray - array from which function creates list of new planes
	 * @return listOfPlanes
	 * @throws IncorrectInputException exception
	 */
	public static ArrayList<PlaneThread> createPlanes(final Object[] bigArray) throws IncorrectInputException {
		/** Variable for logging. */
		final Logger logger = LogManager.getLogger(PlainCreator.class.getName());

		/** Storage for airport. */
		final AirportStorage storage = AirportStorage.getStorage();

		/** Storage for airport. */
		final RouteStorage routeStorage = RouteStorage.getStorage();
		/** list of planes we want to get. */
		final ArrayList<PlaneThread> listOfPlanes = new ArrayList<>();
		/** Random. */
		final Random random = new Random();
		final int t = 5, k1 = 13;
		Object[] planeArray = new Object[t];
		planeArray = (Object[]) bigArray[0];

		Object[] routArray = new Object[t];
		routArray = (Object[]) bigArray[1];

		Object[] stopArray = new Object[k1];
		stopArray = (Object[]) bigArray[2];

		for (int i = 0; i < stopArray.length; i++) {
			final DataForAirport dataForAirport = (DataForAirport) stopArray[i];

			final int b = 16, h = 10;
			final int numberOfPeople = random.nextInt(b) + h;
			final AirportResourses airport = new AirportResourses(dataForAirport.getId(), dataForAirport.getSemaphore(),
					dataForAirport.getName(), 0, numberOfPeople);
			storage.add(airport);

		}

		for (int i = 0; i < routArray.length; i++) {
			final ArrayList<Integer> list = new ArrayList<>();
			int[] lol;
			final ArrayList<AirportResourses> route = new ArrayList<>();

			lol = (int[]) routArray[i];
			for (int k = 0; k < lol.length; k++) {
				list.add(lol[k]);
			}
			final int idOfRoute = list.get(0);
			list.remove(0);
			for (int n = 0; n < list.size(); n++) {
				List<AirportResourses> all = storage.getAll();
				for (int m = 0; m < all.size(); m++) {
					AirportResourses airportResourses = all.get(m);
					if (list.get(n) == airportResourses.getAirportId()) {
						route.add(airportResourses);
					}
				}
			}
			routeStorage.add(idOfRoute, route);
		}

		for (int i = 0; i < planeArray.length; i++) {
			int[] xm;
			ArrayList<Integer> list = new ArrayList<>();
			ArrayList<AirportResourses> route = new ArrayList<>();

			xm = (int[]) planeArray[i];
			for (int k = 0; k < xm.length; k++) {
				list.add(xm[k]);
			}
			logger.debug("number we want  " + list.get(1) + " to plane number " + list.get(0));
			route = routeStorage.searchRout(list.get(1));
			logger.debug("Few first stations of the plane number " + list.get(0) + " is the " + route.get(1).getName()
					+ route.get(2).getName());
			final int a = 300;
			final PlaneThread planeThread = new PlaneThread(list.get(0), list.get(1), list.get(2), a, route, 0);
			listOfPlanes.add(planeThread);
			logger.info("We sussesfuly create the plane number " + list.get(0));
		}

		return listOfPlanes;
	}

}
