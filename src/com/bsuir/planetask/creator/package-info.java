/**
 * This package handles(control) all main functions of this program: read
 * information from file, create point for tetrahedron and perform main task
 * (find V and etc.).
 *
 */
package com.bsuir.planetask.creator;
