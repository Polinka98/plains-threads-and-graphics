/*
 * Parser
 *
 * Version information
 *
 * Date
 *
 * Copyright notice
 */
package com.bsuir.planetask.parser;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bsuir.planetask.airports.DataForAirport;
import com.bsuir.planetask.validation.IncorrectInputException;

/**
 *
 * @author Polina
 *
 */
public final class Parser {
	/** Variable for logging. */
	private static final Logger LOGGER = LogManager.getLogger(Parser.class.getName());

	/**
	 * Constructor for new object.
	 */
	private Parser() {
	}

	/**
	 * Function sniff out validity of data.
	 *
	 * @param array - array with valid planes, airports and routes data
	 *
	 * @return isCorrect shows is line correct or not
	 * @throws IncorrectInputException shows that smth wrong with input data
	 */
	public static Object[] checkFunction(final Object[] array) throws IncorrectInputException {

		final int a = 5, b = 13, c = 3;
		Object[] newArray = new Object[a];
		Object[] newArray2 = new Object[a];
		Object[] newArray3 = new Object[b];

		/** Array will store lines */
		String[] arrForPlane = new String[a];
		String[] arrForRouts = new String[a];
		String[] arrForAirport = new String[b];
		arrForPlane = (String[]) array[0];
		arrForRouts = (String[]) array[1];
		arrForAirport = (String[]) array[2];

		for (int i = 0; i < arrForPlane.length; i++) {
			final ArrayList<String> allValues = new ArrayList<>(Arrays.asList(arrForPlane[i].split(" ")));

			/** Parse lines. */
			final int[] numArr = allValues.stream().mapToInt(Integer::parseInt).toArray();
			newArray[i] = numArr;
		}

		for (int i = 0; i < arrForRouts.length; i++) {
			final ArrayList<String> allValues = new ArrayList<>(Arrays.asList(arrForRouts[i].split(" ")));

			/** Parse lines. */
			final int[] numArr = allValues.stream().mapToInt(Integer::parseInt).toArray();
			newArray2[i] = numArr;
		}

		for (int i = 0; i < arrForAirport.length; i++) {
			final ArrayList<String> allValues = new ArrayList<>(Arrays.asList(arrForAirport[i].split(" ")));

			final String name = allValues.get(1);
			allValues.remove(1);

			/** Parse lines. */
			final int[] numArr = allValues.stream().mapToInt(Integer::parseInt).toArray();
			final DataForAirport dataForStop = new DataForAirport(numArr[0], name, numArr[1]);
			newArray3[i] = dataForStop;
		}
		Object[] bigArray = new Object[c];
		bigArray[0] = newArray;
		bigArray[1] = newArray2;
		bigArray[2] = newArray3;

		return bigArray;
	}

}
