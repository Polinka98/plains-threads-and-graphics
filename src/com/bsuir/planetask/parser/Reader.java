/*
 * MyReader
 *
 * Version information
 *
 * Date
 *
 * Copyright notice
 */
package com.bsuir.planetask.parser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bsuir.planetask.validation.IncorrectInputException;
import com.bsuir.planetask.validation.MyIOException;
import com.bsuir.planetask.validation.ValidationClassForAirport;
import com.bsuir.planetask.validation.ValidationClassForPlane;
import com.bsuir.planetask.validation.ValidationClassForRout;

/**
 *
 * @author Polina
 *
 */
public final class Reader {
	/**
	 * Constructor for new object.
	 */
	private Reader() {
	}

	/**
	 * Function for reading from file.
	 *
	 * @param path  - path to file with plane data
	 * @param path2 - path to file with routes data
	 * @param path3 - path to file with airports data
	 * @return array
	 * @throws MyIOException           exception
	 * @throws IncorrectInputException exception
	 */
	public static Object[] readFromFile(final Path path, final Path path2, final Path path3)
			throws MyIOException, IncorrectInputException {

		/** Variable for logging. */
		final Logger logger = LogManager.getLogger(Reader.class.getName());

		/** List will store data. */
		List<String> lines = new ArrayList<>();

		final int a = 5, b = 13;
		/** Array will store lines */
		String[] arrForPlanes = new String[a];
		String[] arrForRouts = new String[a];
		String[] arrForStops = new String[b];

		try (Stream<String> lineStream = Files.lines(path)) {

			lines = lineStream.collect(Collectors.toList());
		} catch (IOException e) {
			throw new MyIOException("Problems with reading from fail");
		}

		for (int n = 0; n < lines.size(); n++) {
			try {
				if (ValidationClassForPlane.checkFunction(lines.get(n))) {
					arrForPlanes[n] = lines.get(n);
				} else {
					logger.error(" Mistake in file with plane data.");
				}
			} catch (IncorrectInputException e) {
				logger.error(" Mistake in file with plane data.");
				throw e;
			}
		}

		try (Stream<String> lineStream = Files.lines(path2)) {

			lines = lineStream.collect(Collectors.toList());
		} catch (IOException e) {
			throw new MyIOException("Problems with reading from fail");
		}

		for (int n = 0; n < lines.size(); n++) {
			try {
				if (ValidationClassForRout.checkFunction(lines.get(n))) {
					arrForRouts[n] = lines.get(n);
				} else {
					logger.error(" Mistake in file with rout data.");
				}
			} catch (IncorrectInputException e) {
				logger.error(" Mistake in file with rout data.");
				throw e;
			}
		}

		try (Stream<String> lineStream = Files.lines(path3)) {

			lines = lineStream.collect(Collectors.toList());
		} catch (IOException e) {
			throw new MyIOException("Problems with reading from fail");
		}

		for (int n = 0; n < lines.size(); n++) {
			try {
				if (ValidationClassForAirport.checkFunction(lines.get(n))) {
					arrForStops[n] = lines.get(n);
				} else {
					logger.error(" Mistake in file with airport data.");
				}
			} catch (IncorrectInputException e) {
				logger.error(" Mistake in file with airport data.");
				throw e;
			}
		}

		final int p = 3;
		Object[] newArray = new Object[p];
		newArray[0] = arrForPlanes;
		newArray[1] = arrForRouts;
		newArray[2] = arrForStops;

		return newArray;
	}
}
