package com.bsuir.planetask.passenger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bsuir.planetask.planethread.PlaneThread;

import java.util.Random;

/**
 *
 * @author Polina
 *
 */
public class Passenger implements PassengerInterfase<PlaneThread> {
	/** Variable for logging. */
	private static final Logger LOGGER = LogManager.getLogger(Passenger.class.getName());

	/** Constructor. */
	public Passenger() {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void goOutOfThePlane(final PlaneThread key) {
		final Random random = new Random();
		final int a = 15;
		int peopleGoOut = random.nextInt(a);

		if (key.getNumOfPeople() != 0) {
			final int num = key.getNumOfPeople(), num2 = key.getCurrentStop().getNumberOfPeople();
			if (key.getNumOfPeople() >= peopleGoOut) {
				key.setNumOfPeople(key.getNumOfPeople() - peopleGoOut);
				key.getCurrentStop().setNumberOfPeople(key.getCurrentStop().getNumberOfPeople() + peopleGoOut);
				System.out.println(" В самолёте находилось " + num + "  человек. В аэропорту было " + num2
						+ " человек. Из самолёта вышло " + peopleGoOut + " человек. В аэропорте стало "
						+ key.getCurrentStop().getNumberOfPeople() + " человек.");
			} else {
				LOGGER.info("Number of people in the plane is smaller" + " then number of people who want to go out");
				peopleGoOut = key.getNumOfPeople();
				key.setNumOfPeople(key.getNumOfPeople() - peopleGoOut);
				key.getCurrentStop().setNumberOfPeople(key.getCurrentStop().getNumberOfPeople() + peopleGoOut);
				System.out.println("All people leave the plane. ");
			}
		} else {
			LOGGER.info("Plane is empty!");
			System.out.println("Plane is empty!");
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void goToPlane(final PlaneThread key) {
		int num = key.getNumOfPeople(), num2 = key.getCurrentStop().getNumberOfPeople();
		final int a = 15;
		Random random = new Random();
		int peopleGoIn = random.nextInt(a);
		if (key.getNumOfPeople() + peopleGoIn < key.getMaxNumber()) {
			if (key.getCurrentStop().getNumberOfPeople() >= peopleGoIn) {
				key.setNumOfPeople(key.getNumOfPeople() + peopleGoIn);
				key.getCurrentStop().setNumberOfPeople(key.getCurrentStop().getNumberOfPeople() - peopleGoIn);
				System.out.println(" В самолете находилось " + num + " человек. В аэропорту было  " + num2
						+ " человек. В самолёт зашло " + peopleGoIn + " человек. В аэропорту стало "
						+ key.getCurrentStop().getNumberOfPeople() + " человек.");
			} else {
				peopleGoIn = key.getCurrentStop().getNumberOfPeople();
				key.setNumOfPeople(key.getNumOfPeople() + peopleGoIn);
				key.getCurrentStop().setNumberOfPeople(key.getCurrentStop().getNumberOfPeople() - peopleGoIn);
				LOGGER.info("All people from the airport get into the plane");
				System.out.println("All people from the airport get into the plane");
			}
		} else {
			LOGGER.info("To many people in the plane");
			System.out.println("To many people in the plane");
		}
	}

}
