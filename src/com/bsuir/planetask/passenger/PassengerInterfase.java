package com.bsuir.planetask.passenger;

/**
 * Describes generic storage methods.
 *
 * @param <B> Type of objects
 */
public interface PassengerInterfase<B> {

	/**
	 * Adds people to the airport.
	 *
	 * @param key object
	 */
	void goOutOfThePlane(B key);

	/**
	 * Adds people to the plane.
	 *
	 * @param key object
	 */
	void goToPlane(B key);
}
