package com.bsuir.planetask.planethread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bsuir.planetask.airports.AirportResourses;
import com.bsuir.planetask.passenger.Passenger;

/**
 *
 * @author Polina
 *
 */
public class PlaneThread implements Callable<String> {

	/** Variable for logging. */
	private static final Logger LOGGER = LogManager.getLogger(PlaneThread.class.getName());
	/**
	 * Number of plane.
	 */
	private final int number;
	/**
	 * Number of plane route.
	 */
	private final int routeNumber;
	/**
	 * Time to start plane thread.
	 */
	private int startTime;
	/**
	 * Interval for sleeping.
	 */
	private int deltaTime;
	/**
	 * Plane route.
	 */
	private List<AirportResourses> route;
	/**
	 * Number of people in the plane.
	 */
	private int numOfPeople;
	/**
	 * Current airport.
	 */
	private AirportResourses currentStop;

	/** Max number of passengers in the plane. */
	private static final int MAX_NUMBER = 70;

	/**
	 * Constructor for new object.
	 * 
	 * @param number1      Number of plane
	 * @param routeNumber1 Number of plane route
	 * @param startTime1   Time to start plane thread
	 * @param deltaTime1   Interval for sleeping
	 * @param route1       Plane route
	 * @param numOfPeople1 Number of people in the plane
	 */
	public PlaneThread(final int number1, final int routeNumber1, final int startTime1, final int deltaTime1,
			final List<AirportResourses> route1, final int numOfPeople1) {
		this.number = number1;
		this.routeNumber = routeNumber1;
		this.startTime = startTime1;
		this.deltaTime = deltaTime1;
		this.route = route1;
		this.numOfPeople = numOfPeople1;
	}

	/**
	 * Function returns max number of passengers.
	 *
	 * @return MAX_NUMBER of passengers
	 */
	public static int getMaxNumber() {
		return MAX_NUMBER;
	}

	/**
	 * Function returns plane current airport.
	 *
	 * @return currentStop of a plane
	 */
	public AirportResourses getCurrentStop() {
		return currentStop;
	}

	/**
	 * Function sets current airport.
	 *
	 * @param currentAirport1 new airport
	 */
	public void setCurrentStop(final AirportResourses currentAirport1) {
		this.currentStop = currentAirport1;
	}

	/**
	 * Function sets number of people in the plane.
	 *
	 * @param numOfPeople1 new number
	 */
	public void setNumOfPeople(final int numOfPeople1) {
		this.numOfPeople = numOfPeople1;
	}

	/**
	 * Function returns number of people in the plane.
	 *
	 * @return numOfPeople in the plane
	 */
	public int getNumOfPeople() {
		return numOfPeople;
	}

	/**
	 * Function returns plane number.
	 *
	 * @return number of plane
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * Function returns DeltaTime - time interval between stations.
	 *
	 * @return deltaTime time
	 */
	public int getDeltaTime() {
		return deltaTime;
	}

	/**
	 * Function returns route of the plane.
	 *
	 * @return route of the plane
	 */
	public List<AirportResourses> getRoute() {
		return route;
	}

	/**
	 * Function returns number(id) of the route.
	 *
	 * @return routeNumber of the route
	 */
	public int getRouteNumber() {
		return routeNumber;
	}

	/**
	 * Function returns plane start time.
	 *
	 * @return startTime time
	 */
	public int getStartTime() {
		return startTime;
	}

	/**
	 * Function sets time interval between stations.
	 *
	 * @param deltaTime1 time
	 */
	public void setDeltaTime(final int deltaTime1) {
		this.deltaTime = deltaTime1;
	}

	/**
	 * Function sets route of the plane.
	 *
	 * @param route1 of the plane
	 */
	public void setRoute(final ArrayList<AirportResourses> route1) {
		this.route = route1;
	}

	/**
	 * Function sets plane start time.
	 *
	 * @param startTime1 time
	 */
	public void setStartTime(final int startTime1) {
		this.startTime = startTime1;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String call() throws Exception {
		moving();
		return Thread.currentThread().getName();
	}

	/**
	 * Moving function.
	 */
	public void moving() {
		try {
			TimeUnit.MILLISECONDS.sleep(startTime);
		} catch (InterruptedException e) {
			LOGGER.debug("InterruptedException");
		}

		for (int i = 0; i < route.size(); i++) {
			final Semaphore semaphore = new Semaphore(route.get(i).getSemaphore());
			System.out.println("������ ����� " + number + " ������� ���������� ������������ � ��������� "
					+ route.get(i).getName());
			try {
				semaphore.acquire();
				LOGGER.info("Bus get an access");
				System.out.println("������ ����� " + number + " ������� ����������.");

				currentStop = route.get(i);
				route.get(i).setCurrentPlaneCount(1);
				final Passenger transfer = new Passenger();
				transfer.goOutOfThePlane(this);
				transfer.goToPlane(this);
			} catch (InterruptedException e) {
				LOGGER.debug("InterruptedException");
			} finally {
				semaphore.release(); // ?
				System.out.println("������ ����� " + number + " ������� ��������.");
			}
		}

		try {
			TimeUnit.MILLISECONDS.sleep(deltaTime);
		} catch (InterruptedException e) {
			LOGGER.debug("InterruptedException");
		}
	}

}
