/*
 * Main
 *
 * Version information
 *
 * 10.07.2018
 *
 * Copyright notice
 */
package com.bsuir.planetask.runner;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author Polina
 *
 */
public final class Main {

	/**
	 * Constructor for new object.
	 */
	private Main() {
	}

	/**
	 * Function runs program.
	 * 
	 * @param args xa
	 */
	public static void main(final String[] args) {
		final Logger log = LogManager.getLogger(Main.class.getName());
		log.info("work start here!");
		Runner.notMain(args);
	}
}
