/*
 * AssembleClass
 *
 * Version information
 *
 * Date
 *
 * Copyright notice
 */
package com.bsuir.planetask.runner;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bsuir.planetask.creator.PlainCreator;
import com.bsuir.planetask.parser.Parser;
import com.bsuir.planetask.parser.Reader;
import com.bsuir.planetask.planethread.PlaneThread;
import com.bsuir.planetask.validation.IncorrectInputException;
import com.bsuir.planetask.validation.MyIOException;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.stage.Stage;

/**
 *
 * @author Polina
 *
 */
public class Runner extends Application{
	/** Magic number. */
	private final int b = 3;
	/** Variable for logging. */
	private final Logger logger = LogManager.getLogger(Runner.class.getName());
	private final static String PLANE_PATH = "info/plane.txt";
	private final static String ROUTE_PATH = "info/plane_rout.txt";
	private final static String AIRPORT_PATH = "info/airport.txt";

	/** Constructor. */
	public Runner() {
		logger.info("Run");
	}

	/**
	 * Function, witch runs program .
	 */

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {

		AnchorPane root = new AnchorPane();
		root.setPadding(new Insets(5.0));
		Button button = createButton();
		AnchorPane.setBottomAnchor(button, 0.0);
		AnchorPane.setTopAnchor(button, 650.0);
		AnchorPane.setBottomAnchor(button,  0.0);
		AnchorPane.setRightAnchor(button, 300.0);
		AnchorPane.setLeftAnchor(button, 300.0);

		root.getChildren().add(button);

		primaryStage.setScene(new Scene(root,800,700));
		primaryStage.setTitle("Planes");
		primaryStage.getIcons().add(new Image(getResource("").toExternalForm()));
		BackgroundImage image = new BackgroundImage(new Image("kndr.jpg", 645, 695, false, true), BackgroundRepeat.REPEAT,
		BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
		root.setBackground(new Background(image));
		primaryStage.show();
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		}
	}

	public static void notMain (String[] args) {
		launch(args);
	}
	
	private Button createButton() {
		Button result = new Button("Push me");
		result.setCursor(Cursor.HAND);
		result.setOnMouseClicked(mouseEvent -> {

			final Path pathPlane = Paths.get(PLANE_PATH);
			final Path pathRoute = Paths.get(ROUTE_PATH);
			final Path pathAirport = Paths.get(AIRPORT_PATH);
			Object[] newArray = new Object[b];
			try {
				newArray = Reader.readFromFile(pathPlane, pathRoute, pathAirport);
			} catch (MyIOException e) {
				logger.error("error message: " + e.getMessage());
			} catch (IncorrectInputException e) {
				logger.error("error message: " + e.getMessage());
			}

			Object[] bigArray = new Object[b];
			try {
				bigArray = Parser.checkFunction(newArray);
			} catch (IncorrectInputException e) {
				logger.error("error message: " + e.getMessage());
			}
			ArrayList<PlaneThread> listOfPlanes = new ArrayList<>();
			try {
				listOfPlanes = PlainCreator.createPlanes(bigArray);

			} catch (IncorrectInputException e) {
				System.out.println("not ok");
				logger.error("error message: " + e.getMessage());
			}
			System.out.println("number of planes " + listOfPlanes.size());

			ExecutorService executor = Executors.newFixedThreadPool(listOfPlanes.size());

			if (listOfPlanes.size() != 0) {
				executor = Executors.newFixedThreadPool(listOfPlanes.size());
				for (final PlaneThread planeThread : listOfPlanes) {
					executor.submit(planeThread);
				}
				executor.shutdown();

			}
		});
		return result;
	}

	private URL getResource(String name) {
		return getClass().getResource(name);
	}
	
}
