package com.bsuir.planetask.storage;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bsuir.planetask.airports.AirportResourses;

/**
 *
 * @author Polina
 *
 */
public class AirportStorage implements StorageInterface<AirportResourses> {
	/** Variable for logging. */
	private static final Logger LOGGER = LogManager.getLogger(AirportStorage.class.getName());
	/**
	 *
	 * List with all elements of storage.
	 */
	private final List<AirportResourses> storage = new ArrayList<AirportResourses>();

	/**
	 * BusStorage object.
	 */
	private static final AirportStorage STOP_STORAGE = new AirportStorage();

	/**
	 * Get copy of all elements in storage.
	 *
	 * @return copy of a list that contains all elements of storage.
	 */
	public static AirportStorage getStorage() {
		return STOP_STORAGE;
	}

	/**
	 * Get copy of all elements in repository.
	 *
	 * @return copy of a map that contains all elements of repository.
	 */
	public List<AirportResourses> getAll() {
		return new ArrayList<>(storage);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void add(final AirportResourses key) {
		storage.add(key);
		LOGGER.info("We add stop to the storage");

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove(final AirportResourses key) {
		storage.remove(key);
		LOGGER.info("We remove stop from the storage");

	}

}
