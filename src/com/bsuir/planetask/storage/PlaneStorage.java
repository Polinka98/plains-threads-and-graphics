package com.bsuir.planetask.storage;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bsuir.planetask.planethread.PlaneThread;

/**
 *
 * @author Polina
 *
 */
public class PlaneStorage implements StorageInterface<PlaneThread> {
	/** Variable for logging. */
	private static final Logger LOGGER = LogManager.getLogger(PlaneStorage.class.getName());
	/**
	 *
	 * /** List with all elements of storage.
	 */
	private final List<PlaneThread> storage = new ArrayList<PlaneThread>();

	/**
	 * BusStorage object.
	 */
	private static final PlaneStorage BUS_STORAGE = new PlaneStorage();

	/**
	 * Get copy of all elements in storage.
	 *
	 * @return copy of a list that contains all elements of storage.
	 */
	public static PlaneStorage getStorage() {
		return BUS_STORAGE;
	}

	/**
	 * Get copy of all elements in repository.
	 *
	 * @return copy of a map that contains all elements of repository.
	 */
	public List<PlaneThread> getAll() {
		return new ArrayList<>(storage);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void add(final PlaneThread key) {
		storage.add(key);
		LOGGER.info("We add plane number " + key.getNumber() + " to the storage");

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove(final PlaneThread key) {
		storage.remove(key);
		LOGGER.info("We remove plane number " + key.getNumber() + " from the storage");

	}

}
