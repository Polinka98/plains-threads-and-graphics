package com.bsuir.planetask.storage;

import java.util.ArrayList;

import com.bsuir.planetask.airports.AirportResourses;

/**
 * Describes generic storage methods.
 *
 * @param <Integer> - airport id
 * @param <List> - list of airports
 */
public interface RouteInterface<Integer, List> {

	/**
	 * Adds pair key-value to the storage.
	 *
	 * @param key  id
	 * @param list of objects
	 */
	void add(Integer key, List list);

	/**
	 * Removes element by key from storage.
	 *
	 * @param key object to be removed.
	 */
	void remove(Integer key);

	/**
	 * Search rout by key.
	 * 
	 * @param key id
	 * @return result list of airports
	 */
	ArrayList<AirportResourses> searchRout(Integer key);
}
