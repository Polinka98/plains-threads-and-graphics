package com.bsuir.planetask.storage;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bsuir.planetask.airports.AirportResourses;

/**
 *
 * @author Polina
 *
 */
public class RouteStorage implements RouteInterface<Integer, ArrayList<AirportResourses>> {
	/** Variable for logging. */
	private static final Logger LOGGER = LogManager.getLogger(RouteStorage.class.getName());

	/**
	 *
	 * List with all elements of storage.
	 */
	private HashMap<Integer, ArrayList<AirportResourses>> storage = new HashMap<>();

	/**
	 * BusStorage object.
	 */
	private static final RouteStorage ROUTE_STORAGE = new RouteStorage();

	/**
	 * Get copy of all elements in storage.
	 *
	 * @return copy of a list that contains all elements of storage.
	 */
	public static RouteStorage getStorage() {
		return ROUTE_STORAGE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void add(final Integer id, final ArrayList<AirportResourses> list) {
		storage.put(id, list);
		LOGGER.info("Add route to the storage ");

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove(final Integer key) {
		storage.remove(key);
		LOGGER.info("Remove route from the storage ");

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArrayList<AirportResourses> searchRout(final Integer key) {
		// LOGGER.debug("number of route we want to find " + key);
		ArrayList<AirportResourses> result = new ArrayList<>();
		for (final Integer id : storage.keySet()) {
			if (key == id) {
				result = storage.get(key);
				LOGGER.debug("yep!We found it. ");
			}
		}
		return result;
	}
}
