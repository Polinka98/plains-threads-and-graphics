package com.bsuir.planetask.storage;

/**
 * Describes generic storage methods.
 *
 * @param <E> Type of objects which are stored in storage
 */
public interface StorageInterface<E> {

	/**
	 * Adds object to repository.
	 *
	 * @param key object
	 */
	void add(E key);

	/**
	 * Removes element by key from storage.
	 *
	 * @param key object to be removed.
	 */
	void remove(E key);

}
