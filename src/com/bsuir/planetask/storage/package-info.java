/**
 * This package creates storage for airports, planes, routs and describes its
 * main functions.
 */
package com.bsuir.planetask.storage;
