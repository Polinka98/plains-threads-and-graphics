/*
 * IncorrectInputException
 *
 * Version information
 *
 * Date
 *
 * Copyright notice
 */
package com.bsuir.planetask.validation;

/**
 *
 * @author Polina
 *
 */
public class IncorrectInputException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for new object.
	 *
	 * @param text line
	 *
	 */
	public IncorrectInputException(final String text) {
		super(text);
	}
}
