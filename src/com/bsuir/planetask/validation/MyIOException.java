/*
 * MyIOException
 *
 * Version information
 *
 * Date
 *
 * Copyright notice
 */
package com.bsuir.planetask.validation;

/**
 *
 * @author Polina
 *
 */
public class MyIOException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for new object.
	 *
	 * @param text line
	 *
	 */
	public MyIOException(final String text) {
		super(text);
	}
}
