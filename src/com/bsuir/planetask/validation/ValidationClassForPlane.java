/*
 * CheckClass
 *
 * Version information
 *
 * Date
 *
 * Copyright notice
 */
package com.bsuir.planetask.validation;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Polina
 *
 */
public final class ValidationClassForPlane {

	/** Constant for regular statement. */
	private static final String REG2 = "^[\\d+\\s]+";

	/** Variable for logging. */
	private static final Logger LOGGER = LogManager.getLogger(ValidationClassForPlane.class.getName());

	/**
	 * Constructor for new object.
	 */
	private ValidationClassForPlane() {
	}

	/**
	 * Function sniff out validity of data.
	 *
	 * @param brr - line with data
	 *
	 * @return isCorrect shows is line correct or not
	 * @throws IncorrectInputException shows that smth wrong with input data
	 */
	public static boolean checkFunction(final String brr) throws IncorrectInputException {

		/** Variable shows is the line correct or not. */
		boolean isCorrect = true;
		String line = brr;
		Pattern p = Pattern.compile(REG2);
		Matcher m = p.matcher(line);
		if (!(m.matches())) {
			isCorrect = false;
			LOGGER.error(" somth wrong ");
			throw new IncorrectInputException("Incorrect input");
		}

		return isCorrect;
	}
}
