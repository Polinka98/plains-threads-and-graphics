/**
 * This package handles(control) data validation: correct input, correct data,
 * exceptions and etc.
 *
 */
package com.bsuir.planetask.validation;
